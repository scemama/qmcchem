#!/bin/bash 

set -e
set -u
QMCCHEM_PATH=$PWD
mkdir -p "${QMCCHEM_PATH}"/bin
cd "${QMCCHEM_PATH}"/install
mkdir -p Downloads _build

cd "${QMCCHEM_PATH}"

set +e
if [[ ! -f make.config ]]
then
  which ifort > /dev/null
  if [[ $? ]] 
  then
    cp make.config.ifort make.config
  else
    cp make.config.gfortran make.config
  fi
fi

echo "====================================================================="
echo "Configuration OK."
echo "Now, source the qmcchemrc file and build the program:"
echo ""
echo "source qmcchemrc"
echo "make"
echo ""
echo "====================================================================="

if [[ -f qmcchemrc ]] ; then
  cp qmcchemrc qmcchemrc.bak
fi
cat << EOF > qmcchemrc                                                                        
# QMC=Chem environment variables
export QMCCHEM_PATH=${QMCCHEM_PATH}
export PATH="\${QMCCHEM_PATH}/bin:\${PATH}"
export LD_LIBRARY_PATH="\${QMCCHEM_PATH}/lib:\${LD_LIBRARY_PATH}"
export LIBRARY_PATH="\${QMCCHEM_PATH}/lib:\${LIBRARY_PATH}"
export QMCCHEM_MPIRUN="mpirun"
export QMCCHEM_MPIRUN_FLAGS=""
#export QMCCHEM_IO="B"
export C_INCLUDE_PATH="\${QMCCHEM_PATH}/include:\${C_INCLUDE_PATH}"
#export QMCCHEM_NIC=ib0
source \${QMCCHEM_PATH}/irpf90/bin/irpman
#source \${QMCCHEM_PATH}/EZFIO/Bash/ezfio.sh
eval \$(opam env)
EOF

