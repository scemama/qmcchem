#!/bin/bash 

if [[ ! -f qmcchemrc ]]
then
  echo "Error: qmcchemrc not found"
  exit -1
fi

source make.config
source qmcchemrc
FCFLAGS="${FCFLAGS} -fPIC"
export IRPF90 FC FCFLAGS AR RANLIB
cd EZFIO/config
[[ -f properties.config ]]  || ln -s ../../ezfio_config/properties.config .
[[ -f qmc.config ]]  || ln -s ../../ezfio_config/qmc.config .
cd ..
source ../make.config 
make || exit -1
cp lib/libezfio{,_irp}.a ${QMCCHEM_PATH}/lib/ || exit 1


