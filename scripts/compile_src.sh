#!/bin/bash

if [[ -z ${QMCCHEM_PATH} ]]
then
  echo "Error: qmcchemrc not loaded"
  exit -1
fi


cd ${QMCCHEM_PATH}/src || exit -1


source ${QMCCHEM_PATH}/make.config

LIB="${QMCCHEM_PATH}/lib/libezfio_irp.a ${QMCCHEM_PATH}/lib/libf77zmq.a ${QMCCHEM_PATH}/lib/libzmq.a -lstdc++ -lrt -lz ${LIB}"
SRC="${SRC} ZMQ/f77_zmq_module.f90"
OBJ="${OBJ} IRPF90_temp/ZMQ/f77_zmq_module.o"
INCLUDES="${INCLUDES} -I AO -I SAMPLING -I TOOLS -I JASTROW -I MAIN -I PROPERTIES -I ZMQ"
IRPF90_FLAGS="${IRPF90_FLAGS} ${INCLUDES}"


export IRPF90 IRPF90_FLAGS INCLUDES LIB SRC OBJ 

exec make ${@}
